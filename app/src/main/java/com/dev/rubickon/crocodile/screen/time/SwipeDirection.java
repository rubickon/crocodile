package com.dev.rubickon.crocodile.screen.time;

/**
 * Created by DNS1 on 18.07.2017.
 */

public enum SwipeDirection {
    ALL, LEFT, RIGHT, NONE;
}
