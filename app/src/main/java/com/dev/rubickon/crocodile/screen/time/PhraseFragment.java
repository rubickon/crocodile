package com.dev.rubickon.crocodile.screen.time;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.rubickon.crocodile.R;
import com.dev.rubickon.crocodile.utils.AutoResizeTextView;
import com.dev.rubickon.crocodile.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhraseFragment extends Fragment {

    @BindView(R.id.tv_time)
    AutoResizeTextView textView;


    public static PhraseFragment newInstance(String phrase) {
        PhraseFragment fragment = new PhraseFragment();
        Bundle args = new Bundle();
        args.putString(Constants.PHRASES, phrase);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_phrase, container, false);
        ButterKnife.bind(this, view);
        String phrase = getArguments().getString(Constants.PHRASES);
        textView.setText(phrase);
        return view;
    }

}
