package com.dev.rubickon.crocodile.screen.time;

import android.support.annotation.NonNull;

import com.dev.rubickon.crocodile.model.Phrase;
import com.dev.rubickon.crocodile.screen.common.LoadingView;

import java.util.List;

/**
 * Created by DNS1 on 17.07.2017.
 */

public interface PhraseView extends LoadingView {

    void showItems(@NonNull List<Phrase> items);

    void showError(Throwable throwable);

}
