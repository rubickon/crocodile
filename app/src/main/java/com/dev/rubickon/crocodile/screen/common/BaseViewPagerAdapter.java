package com.dev.rubickon.crocodile.screen.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DNS1 on 17.07.2017.
 */

public  abstract class BaseViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();


    public BaseViewPagerAdapter(FragmentManager manager) {
        super(manager);

    }

    public List<Fragment> getmFragmentList() {
        return mFragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    public void addFrag(Fragment fragment) {
        mFragmentList.add(fragment);
    }
}

