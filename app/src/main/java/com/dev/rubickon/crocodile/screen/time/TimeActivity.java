package com.dev.rubickon.crocodile.screen.time;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.rubickon.crocodile.R;
import com.dev.rubickon.crocodile.model.Phrase;
import com.dev.rubickon.crocodile.screen.BaseActivity;
import com.dev.rubickon.crocodile.screen.common.BaseViewPagerAdapter;
import com.dev.rubickon.crocodile.screen.common.LoadingDialog;
import com.dev.rubickon.crocodile.screen.common.LoadingView;
import com.dev.rubickon.crocodile.utils.Constants;
import com.dev.rubickon.crocodile.utils.PlayPauseView;
import com.dev.rubickon.crocodile.utils.PreferencesManager;
import com.dev.rubickon.crocodile.utils.ViewPagerAnimation;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class TimeActivity extends BaseActivity implements PhraseView {

    @BindView(R.id.vp_time)
    CustomViewPager mViewPager;
    @BindView(R.id.toolbar_time)
    Toolbar mTimeToolbar;
    @BindView(R.id.tv_timer)
    TextView mTvTimer;
    @BindView(R.id.btn_skip)
    Button mBtSkip;
    @BindView(R.id.btn_next)
    Button mBtNext;
    @BindView(R.id.tv_score_one)
    TextView mTvScoreOne;
    @BindView(R.id.tv_score_two)
    TextView mTvScoreTwo;
    @BindView(R.id.play_pause_view)
    PlayPauseView mPPView;

    private AppBarLayout appbar;
    private ReportViewPagerAdapter adapter;
    private PhrasePresenter presenter;
    private LoadingView mLoadingView;
    private ViewPagerAnimation vpa;

    private Disposable subTimer;
    private Observable<Long> obsTimer;
    private boolean isPause = true;

    private int scoreOne;
    private int scoreTwo;
    private Long time;
    private boolean step = true;
    private boolean turnEnd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.activity_content);
        getLayoutInflater().inflate(R.layout.activity_time, contentFrameLayout);
        ButterKnife.bind(this);
        /*init AppBar*/
        initAppBar(mTimeToolbar);
        setParentBackArrow(mTimeToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            setAppBarSize(getAppBar(), getResources().getInteger(R.integer.app_bar_size));
        }
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());
        vpa = new ViewPagerAnimation();
        /*get phrases from server*/
        String level = getIntent().getStringExtra(Constants.LEVEL_EXTRAS);
        adapter = new ReportViewPagerAdapter(getSupportFragmentManager());
        presenter = new PhrasePresenter(this);
        presenter.init(level);
        /*restore params*/
        if (savedInstanceState != null) {
            time = savedInstanceState.getLong(Constants.TIMER_TIME_KEY);
            scoreOne = savedInstanceState.getInt(Constants.SCORE_ONE_KEY);
            scoreTwo = savedInstanceState.getInt(Constants.SCORE_TWO_KEY);
            mTvScoreOne.setText(String.valueOf(scoreOne));
            mTvScoreTwo.setText(String.valueOf(scoreTwo));
        } else {
            time = Long.valueOf(PreferencesManager.getTime(this));
            scoreOne = 0;
            scoreTwo = 0;
        }
        /*init timer*/
        timer(time);
        mPPView.toggle();
        mTvTimer.setText(String.valueOf(time));
        mTvTimer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                swipeStep(s);
            }
        });
        /*button listeners*/
        mBtSkip.setOnClickListener(v -> skip());
        mBtNext.setOnClickListener(v -> next());
        mPPView.setOnClickListener(v -> animatePlayPause());
    }

    @Override
    public void hideLoading() {
        mLoadingView.hideLoading();
    }

    @Override
    public void showLoading(Disposable disposable) {
        mLoadingView.showLoading(disposable);
    }

    @Override
    public void showItems(@NonNull List<Phrase> items) {
        adapter.setViewPager(mViewPager, items, this);
    }

    @Override
    public void showError(Throwable throwable) {
        if (throwable.getMessage().equals("Unable to resolve host \"pitsmap.ru\": No address associated with hostname")) {
            Toast.makeText(this, getResources().getString(R.string.connect_error), Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(Constants.TIMER_TIME_KEY, Long.valueOf(mTvTimer.getText().toString()));
        outState.putInt(Constants.SCORE_TWO_KEY, scoreTwo);
        outState.putInt(Constants.SCORE_ONE_KEY, scoreOne);
    }


    private void pause() {
        subTimer.dispose();
    }

    private void resume() {
        timer(Long.valueOf(mTvTimer.getText().toString()));
        subTimer = obsTimer.subscribe(result -> mTvTimer.setText(String.valueOf(result)));
    }

    private void timer(Long time) {
        obsTimer = Observable
                .interval(1, TimeUnit.SECONDS)
                .take(time + 1) // 0, 1, 2, ..., time1
                .map(v -> time - v) // time - 0, time - 1, .. , time - time1
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void animatePlayPause() {
        if (isPause) {
            resume();
            isPause = false;
        } else {
            pause();
            isPause = true;
        }
        mPPView.toggle();
        if (turnEnd){
            skip();
        }
    }


    private void next() {
        if (adapter.getCount() != 0) {
            animateViewPager();
            if (!turnEnd){
                if (step){
                    scoreOne++;
                    mTvScoreOne.setText(String.valueOf(scoreOne));
                } else {
                    scoreTwo++;
                    mTvScoreTwo.setText(String.valueOf(scoreTwo));
                }
            } else {
                skip();
            }
        }
    }

    private void skip() {
        if (adapter.getCount() != 0) {
            animateViewPager();
            turnEnd = false;
        }
    }

    private void animateViewPager() {
        vpa.animate(mViewPager);
        if (isPause) {
            resume();
            isPause = false;
            mPPView.toggle();
        }
    }

    private void swipeStep(Editable s){
        if (s.toString().equals("0")) {
            pause();
            isPause = true;
            turnEnd = true;
            mPPView.toggle();
            step = !step;
            time = Long.valueOf(PreferencesManager.getTime(getApplicationContext()));
            timer(time);
            mTvTimer.setText(String.valueOf(time));
            if (step) {
                mTvScoreOne.setTextColor(getResources().getColor(R.color.icons));
                mTvScoreTwo.setTextColor(getResources().getColor(R.color.secondary_text));
            } else {
                mTvScoreOne.setTextColor(getResources().getColor(R.color.secondary_text));
                mTvScoreTwo.setTextColor(getResources().getColor(R.color.icons));
            }
        }
    }

    @Nullable
    protected AppBarLayout getAppBar() {
        if (appbar == null) appbar = (AppBarLayout) findViewById(R.id.app_bar_time);
        return appbar;
    }


    public class ReportViewPagerAdapter extends BaseViewPagerAdapter {

        public ReportViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        private void setViewPager(CustomViewPager viewPager, List<Phrase> items, Activity activity) {
            /*init first two fragments in viewpager*/
            for (int i = 0; i < 2; i++) {
                int randomSize = items.size();
                int index = (int) (Math.random() * randomSize);
                PhraseFragment phraseFragment = PhraseFragment.newInstance(items.get(index).getPhrase());
                adapter.addFrag(phraseFragment);
                items.remove(index);
            }
            viewPager.setAllowedSwipeDirection(SwipeDirection.RIGHT);
            viewPager.setOffscreenPageLimit(1);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (items.size() != 0) {
                        /*after swipe take one random element from phrases array and init them in viewpager*/
                        int randomSize = items.size();
                        int index = (int) (Math.random() * randomSize);
                        PhraseFragment phraseFragment = PhraseFragment.newInstance(items.get(index).getPhrase());
                        adapter.addFrag(phraseFragment);
                        notifyDataSetChanged();
                        /*remove this element in phrases array*/
                        items.remove(index);
                    } else if (items.size() == 0) {
                        /*if phrases array empty show warning message to user*/
                        Toast.makeText(activity, getResources().getString(R.string.end_phrases), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            viewPager.setAdapter(adapter);
        }
    }

}
