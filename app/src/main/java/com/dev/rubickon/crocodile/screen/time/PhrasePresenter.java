package com.dev.rubickon.crocodile.screen.time;

import com.dev.rubickon.crocodile.repository.RepositoryProvider;

/**
 * Created by DNS1 on 17.07.2017.
 */

public class PhrasePresenter {

    private PhraseView mView;

    public PhrasePresenter(PhraseView mView) {
        this.mView = mView;
    }


    public void init(String level) {
        RepositoryProvider.providePhraseRepository()
                .phrases(level)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(mView::showItems, mView::showError);
    }

}
