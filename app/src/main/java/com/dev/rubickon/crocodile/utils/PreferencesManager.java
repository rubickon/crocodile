package com.dev.rubickon.crocodile.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;

import com.dev.rubickon.crocodile.screen.settings.SettingsFragment;

/**
 * Created by DNS1 on 19.07.2017.
 */

public class PreferencesManager {

    @Nullable
    public static String getTime(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String time = sharedPref.getString(Constants.PREF_TIME, "60");
        if (time.equals("-1"))
            time = sharedPref.getString(Constants.ET_PREF_TIME, "60");
        return time;
    }

}
