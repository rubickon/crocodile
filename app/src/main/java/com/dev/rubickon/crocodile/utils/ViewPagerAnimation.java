package com.dev.rubickon.crocodile.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.animation.LinearInterpolator;

import com.dev.rubickon.crocodile.screen.time.CustomViewPager;

/**
 * Created by DNS1 on 21.07.2017.
 */

public class ViewPagerAnimation implements Animator.AnimatorListener {


    private boolean mIsInAnimation;
    private long mMotionBeginTime;
    private float mLastMotionX;
    private CustomViewPager mViewPager;


    public void animate(CustomViewPager viewPager) {
        mViewPager = viewPager;
        if (mIsInAnimation)
            return;
        ObjectAnimator anim;
        if (!hasNextPage())
            return;
        anim = ObjectAnimator.ofFloat(this, "motionX", 0, -mViewPager.getWidth());

        anim.setInterpolator(new LinearInterpolator());
        anim.addListener(this);
        anim.setDuration(300);
        anim.start();
    }

    public void setMotionX(float motionX) {
        if (!mIsInAnimation) return;
        mLastMotionX = motionX;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_MOVE, mMotionBeginTime, time);
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mIsInAnimation = false;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_UP, mMotionBeginTime, time);
    }


    @Override
    public void onAnimationStart(Animator animation) {
        mLastMotionX = 0;
        mIsInAnimation = true;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_DOWN, time, time);
        mMotionBeginTime = time;
    }

    // method from http://stackoverflow.com/a/11599282/1294681
    private void simulate(int action, long startTime, long endTime) {
        // specify the property for the two touch points
        MotionEvent.PointerProperties[] properties = new MotionEvent.PointerProperties[1];
        MotionEvent.PointerProperties pp = new MotionEvent.PointerProperties();
        pp.id = 0;
        pp.toolType = MotionEvent.TOOL_TYPE_FINGER;

        properties[0] = pp;

        // specify the coordinations of the two touch points
        // NOTE: you MUST set the pressure and size value, or it doesn't work
        MotionEvent.PointerCoords[] pointerCoords = new MotionEvent.PointerCoords[1];
        MotionEvent.PointerCoords pc = new MotionEvent.PointerCoords();
        pc.x = mLastMotionX;
        pc.pressure = 1;
        pc.size = 1;
        pointerCoords[0] = pc;

        final MotionEvent ev = MotionEvent.obtain(
                startTime, endTime, action, 1, properties,
                pointerCoords, 0, 0, 1, 1, 0, 0, 0, 0);

        mViewPager.dispatchTouchEvent(ev);
    }

    private boolean hasNextPage() {
        return mViewPager.getCurrentItem() + 1 < mViewPager.getAdapter().getCount();
    }


    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

}
