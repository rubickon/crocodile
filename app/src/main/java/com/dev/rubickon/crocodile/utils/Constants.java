package com.dev.rubickon.crocodile.utils;

/**
 * Created by DNS1 on 10.07.2017.
 */

public final class Constants {

    public static final String BASE_URL = "http://pitsmap.ru/croc/";

    public static final String LEVEL_EXTRAS = "level";

    public static final String DIALOG_KEY = "send_response";

    public static final String TIMER_TIME_KEY = "timer_time";

    public static final String SCORE_ONE_KEY = "score_one";

    public static final String SCORE_TWO_KEY = "score_two";

    public static final String PHRASES = "phrases";

    public static final String PREF_TIME =  "pref_time";

    public static final String ET_PREF_TIME = "et_pref_time";

}
